import h5py
import numpy as np
from decorator import decorator
from pyTrajectory.config import cfg


cfg.trajectory_keys = ('pred_boxes',
                       'pred_classes',
                       'pred_keypoint_heatmaps',
                       'pred_keypoints',
                       'scores',
                       'time_stamp')

cfg.key_time_stamp = 'time_stamp'
cfg.key_category = 'pred_classes'
cfg.key_score = 'scores'
cfg.key_box = 'pred_boxes'
cfg.key_keypoints = 'pred_keypoints'
cfg.key_keypoints_line = 'pred_keypoints'


from pyTrajectory.trajectory import Trajectory


def save_trajectories(trajectories, trajectory_file):
    with h5py.File(trajectory_file, 'w') as h5_file:
        for identity in trajectories:
            trajectory_group = h5_file.create_group(str(identity))
            for key, value in trajectories[identity].items():
                if value is None:
                    continue
                trajectory_group.create_dataset(key, data=value)


def load_trajectories(trajectory_file):
    trajectories = {}
    with h5py.File(trajectory_file, 'r') as h5_file:
        for identity in h5_file:
            h5_data = h5_file[identity].items()
            data = {}
            for key, value in h5_data:
                data[key] = value[:]
            trajectories[int(identity)] = Trajectory().load(data=data)
    return trajectories


def identity_lookup(trajectories):
    frames = sum(trajectories.values(), Trajectory())[cfg.key_time_stamp]
    frames = np.arange(0, frames.max() + 1)
    lookup = np.transpose([np.isin(frames, trajectory[cfg.key_time_stamp]) \
                           for trajectory in trajectories.values()])
    return lookup

@decorator
def return_lookup(func, *args, **kwargs):
    trajectories = func(*args, **kwargs)
    return trajectories, identity_lookup(trajectories)

def split_trajectory(trajectory, frame):
    start = trajectory[0][cfg.key_time_stamp]
    end = trajectory[-1][cfg.key_time_stamp]
    if frame <= start:
        return Trajectory([]), trajectory
    if frame > end:
        return trajectory, Trajectory([])
    return trajectory.slice_window(start, frame - 1, False), trajectory.slice_window(frame, end, False)

def check_trajectory_overlap(trajectory_1, trajectory_2):
    return np.isin(trajectory_1[cfg.key_time_stamp],
                   trajectory_2[cfg.key_time_stamp]).any()

@return_lookup
def delete_trajectories(trajectories, identities, lookup=None):
    for identity in identities:
        _ = trajectories.pop(identity)
    return trajectories

@return_lookup
def add_trajectories(trajectories, new_trajectories, lookup=None):
    new_identities = np.arange(len(new_trajectories)) + max(trajectories) + 1
    for identity, trajectory in zip(new_identities, new_trajectories):
        trajectories[identity] = trajectory
    return trajectories

@return_lookup
def split_trajectories(trajectories, identities, frame, remove_empty=True, lookup=None):
    trajectories_pre = []
    trajectories_post = []
    for identity in identities:
        trajectory_pre, trajectory_post = split_trajectory(trajectories[identity], frame)
        trajectories_pre.append(trajectory_pre)
        trajectories_post.append(trajectory_post)
    for identity, trajectory_pre in zip(identities, trajectories_pre):
        if remove_empty and len(trajectory_pre) == 0:
            _ = trajectories.pop(identity)
            continue
        trajectories[identity] = trajectory_pre
    if remove_empty:
        trajectories_post = [trajectory for trajectory in trajectories_post if len(trajectory) > 0]
    trajectories, _ = add_trajectories(trajectories, trajectories_post)
    return trajectories

@return_lookup
def switch_trajectories(trajectories, identities, frame, lookup=None):
    if len(identities) != 2:
        return trajectories
    trajectories, _ = split_trajectories(trajectories, identities, frame, remove_empty=False)
    # last two are new trajectories
    new_identities = list(trajectories.keys())[-2:][::-1]
    for identity, new_identity in zip(identities, new_identities):
        trajectories[identity] = (trajectories[identity] + trajectories.pop(new_identity)).sort()
    return trajectories

@return_lookup
def merge_trajectories(trajectories, identities, lookup=None):
    identity = identities[0]
    merge_identities = identities[1:]
    for merge_identity in merge_identities:
        # do not allow overlapping frames
        if check_trajectory_overlap(trajectories[identity], trajectories[merge_identity]):
            return trajectories
    for merge_identity in merge_identities:
        trajectories[identity] = (trajectories[identity] + trajectories.pop(merge_identity)).sort()
    return trajectories

def delete_short_trajectories(trajectories, min_length):
    for identity in list(trajectories.keys()):
        if len(trajectories[identity]) > min_length:
            _ = trajectories.pop(identity)
    return trajectories
